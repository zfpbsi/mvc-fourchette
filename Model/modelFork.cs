﻿/*
 * Bertrand Sahli
 * ETML
 * Novembre 2017
 * Mise en oeuvre du paradigme MVC en exploitant la problématique de la fourchette 
 * => class ModelFork pour gérer les données de cette application.
 */
using System;

namespace MVC_Fourchette_BSI
{
    public class ModelFork
    {
        // Déclaration des données de l'application et leurs accesseurs
        private int _min;
        public int Min
        {
            get { return _min; }
            set { _min = value; }
        }

        private int _max;
        public int Max
        {
            get { return _max; }
            set { _max = value; }
        }

        private int _value;
        public  int Value
        {
            get { return _value; }
        }

        private Random _alea = new Random();

        /// <summary>
        /// Constructeur du modèle qui initialise les données de l'application
        /// et assure la liaison entre le modèle et le contrôleur
        /// </summary>
        /// <param name="control"></param>
        public ModelFork(CtrlFork control)
        {
            control.Model = this;
            Init();
        }

        /// <summary>
        /// Initialise les données du modèle
        /// </summary>
        public void Init()
        {
            _max = 500;
            _min = 0;
            _value = _alea.Next(_max + 1);
        }
    }
}
