﻿/*
 * Bertrand Sahli
 * ETML
 * Novembre 2017
 * Mise en oeuvre du paradigme MVC en exploitant la problématique de la fourchette 
 * => class CtrlFork pour orchestrer les événements, les actions, etc. de cette application
 */
 using System;

namespace MVC_Fourchette_BSI
{
    public class CtrlFork
    {
        // Pour accéder en L/E la vue et le modèle
        private ModelFork _model;
        public  ModelFork Model
        {
            get { return _model; }
            set { _model = value; }
        }

        private FrmFork _view;
        public FrmFork View
        {
            get { return _view; }
            set { _view = value; }
        }

        /// <summary>
        /// Constructeur par défaut ... qui ne fait rien de particulier
        /// </summary>
        public CtrlFork()
        {
        }

        /// <summary>
        /// Lorsque le contrôleur le prévoit, il faut réinitialiser la vue et le modèle
        /// </summary>
        public void Init()
        {
            // Fixe les valeurs initiales du model
            _model.Init();

            // Fixe les affichages à leur valeurs initales
            _view.Init(_model.Min, _model.Max);
        }

        /// <summary>
        /// Vérifie la saisie opérée
        /// </summary>
        /// <param name="guess"></param>
        public void SendValue(string guess)
        {
            int value = 0;
            try
            {
                value = Convert.ToInt32(guess);
            }
            catch
            {
                _view.DisplayMsg("Saisie erronée", -1, -1, false);
                return;
            }

            if (_model.Min > value || _model.Max < value)
            {
                _view.DisplayMsg("Saisie hors limite", -1, -1, false);
                return;
            }

            CheckGuess(value);
        }

        /// <summary>
        /// Si la saisie est correcte, vérifie la proposition
        /// </summary>
        /// <param name="guess"></param>
        private void CheckGuess(int guess)
        {
            if (guess == _model.Value)
            {
                _view.DisplayMsg("Excellent: " + guess.ToString(), -1, -1, true);
                return;
            }

            if (guess > _model.Value)
            {
                _view.DisplayMsg("Trop haut: " + guess.ToString(), -1, guess, false);
                _model.Max = guess;
                return;
            }

            if (guess < _model.Value)
            {
                _view.DisplayMsg("Trop bas: " + guess.ToString(), guess, -1, false);
                _model.Min = guess;
                return;
            }
        }
    }
}
