﻿/*
 * Bertrand Sahli
 * ETML
 * Novembre 2017
 * Mise en oeuvre du paradigme MVC en exploitant la problématique de la fourchette 
 * => class FrmFork pour afficher l'HIM de cette application.
 */

using System;
using System.Windows.Forms;

namespace MVC_Fourchette_BSI
{
    public partial class FrmFork : Form
    {
        // Les 2 textes admissibles sur le bouton d'action
        private const string STR_GUESS = "Vérifier";
        private const string STR_REDO  = "Recommencer";

        // Pour assurer le lien entre la vue et le contrôleur
        private CtrlFork control;
        private FrmFork maVue;
        /// <summary>
        /// Constructeur de la vue 
        /// </summary>
        public FrmFork(CtrlFork control)
        {
            // Links Control & View
            this.control = control;
            control.View = this;

            // Prepares the UI
            InitializeComponent();
        }

        /// <summary>
        /// Gère les click de l'utilisateur sur le bouton d'action:
        /// a) tester une proposition
        /// b) recommencer la fourchette
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTry_Click(object sender, EventArgs e)
        {
            if (btnTry.Text == STR_GUESS)
                control.SendValue(tbxCheck.Text);
            else if (btnTry.Text == STR_REDO)
                control.Init();
        }

        /// <summary>
        /// Pour initialiser les affichages (sur l'impulsion du contrôleur)
        /// </summary>
        public void Init(int min, int max)
        {
            btnTry.Text = STR_GUESS;
            tbxCheck.Text = "?";
            tbxMax.Text = max.ToString();
            tbxMin.Text = min.ToString();
        }

        /// <summary>
        /// Afin d'afficher les informations induites par le contrôleur 
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="minVal"></param>
        /// <param name="maxVal"></param>
        /// <param name="victory"></param>
        public void DisplayMsg(string msg, int minVal, int maxVal, bool victory)
        {
            if (msg != "")
                tbxCheck.Text = msg;

            if (minVal != -1)
                tbxMin.Text = minVal.ToString();

            if (maxVal != -1)
                tbxMax.Text = maxVal.ToString();

            if (victory)
                btnTry.Text = STR_REDO;
        }

        /// <summary>
        /// /// Gère la frappe de [ENTER] dans la zone de saisie afin de:
        /// a) tester une proposition
        /// b) recommencer la fourchette
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbxCheck_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
                btnTry_Click(null, null);
        }
    }
}
