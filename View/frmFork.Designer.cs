﻿/*
 * Bertrand Sahli
 * ETML
 * Novembre 2017
 * Mise en oeuvre du paradigme MVC en exploitant la problématique de la fourchette 
 * => class FrmFork pour afficher l'HIM de cette application.
 */
namespace MVC_Fourchette_BSI
{
    partial class FrmFork
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblForkGame = new System.Windows.Forms.Label();
            this.lblMin = new System.Windows.Forms.Label();
            this.lblMax = new System.Windows.Forms.Label();
            this.btnTry = new System.Windows.Forms.Button();
            this.tbxMin = new System.Windows.Forms.TextBox();
            this.tbxMax = new System.Windows.Forms.TextBox();
            this.tbxCheck = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblForkGame
            // 
            this.lblForkGame.AutoEllipsis = true;
            this.lblForkGame.AutoSize = true;
            this.lblForkGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForkGame.Location = new System.Drawing.Point(12, 9);
            this.lblForkGame.Name = "lblForkGame";
            this.lblForkGame.Size = new System.Drawing.Size(389, 78);
            this.lblForkGame.TabIndex = 0;
            this.lblForkGame.Text = "Bienvenue dans le programme \"Fourchette\" !\r\n\r\nSaisissez un nombre compris entre l" +
    "es deux limites\r\nindiquées et tentez de deviner le nombre mystère.";
            this.lblForkGame.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblForkGame.UseCompatibleTextRendering = true;
            // 
            // lblMin
            // 
            this.lblMin.AutoSize = true;
            this.lblMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMin.Location = new System.Drawing.Point(26, 106);
            this.lblMin.Name = "lblMin";
            this.lblMin.Size = new System.Drawing.Size(37, 20);
            this.lblMin.TabIndex = 1;
            this.lblMin.Text = "Min";
            // 
            // lblMax
            // 
            this.lblMax.AutoSize = true;
            this.lblMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMax.Location = new System.Drawing.Point(342, 106);
            this.lblMax.Name = "lblMax";
            this.lblMax.Size = new System.Drawing.Size(41, 20);
            this.lblMax.TabIndex = 2;
            this.lblMax.Text = "Max";
            // 
            // btnTry
            // 
            this.btnTry.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTry.Location = new System.Drawing.Point(86, 179);
            this.btnTry.Name = "btnTry";
            this.btnTry.Size = new System.Drawing.Size(238, 45);
            this.btnTry.TabIndex = 3;
            this.btnTry.Text = "Vérifier";
            this.btnTry.UseVisualStyleBackColor = true;
            this.btnTry.Click += new System.EventHandler(this.btnTry_Click);
            // 
            // tbxMin
            // 
            this.tbxMin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbxMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxMin.Location = new System.Drawing.Point(30, 130);
            this.tbxMin.Name = "tbxMin";
            this.tbxMin.ReadOnly = true;
            this.tbxMin.Size = new System.Drawing.Size(50, 26);
            this.tbxMin.TabIndex = 4;
            this.tbxMin.Text = "0";
            this.tbxMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbxMax
            // 
            this.tbxMax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbxMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxMax.Location = new System.Drawing.Point(330, 130);
            this.tbxMax.Name = "tbxMax";
            this.tbxMax.ReadOnly = true;
            this.tbxMax.Size = new System.Drawing.Size(50, 26);
            this.tbxMax.TabIndex = 5;
            this.tbxMax.Text = "100";
            this.tbxMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbxCheck
            // 
            this.tbxCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxCheck.Location = new System.Drawing.Point(86, 129);
            this.tbxCheck.Name = "tbxCheck";
            this.tbxCheck.Size = new System.Drawing.Size(238, 26);
            this.tbxCheck.TabIndex = 6;
            this.tbxCheck.Text = "?";
            this.tbxCheck.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbxCheck.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbxCheck_KeyDown);
            // 
            // FrmFork
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(410, 242);
            this.Controls.Add(this.tbxCheck);
            this.Controls.Add(this.tbxMax);
            this.Controls.Add(this.tbxMin);
            this.Controls.Add(this.btnTry);
            this.Controls.Add(this.lblMax);
            this.Controls.Add(this.lblMin);
            this.Controls.Add(this.lblForkGame);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(426, 280);
            this.MinimumSize = new System.Drawing.Size(426, 280);
            this.Name = "FrmFork";
            this.Text = "Fourchette - MVC | BSI @ ETML";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblForkGame;
        private System.Windows.Forms.Label lblMin;
        private System.Windows.Forms.Label lblMax;
        private System.Windows.Forms.Button btnTry;
        private System.Windows.Forms.TextBox tbxMin;
        private System.Windows.Forms.TextBox tbxMax;
        private System.Windows.Forms.TextBox tbxCheck;
    }
}

