﻿/*
 * Bertrand Sahli
 * ETML
 * Novembre 2017
 * Mise en oeuvre du paradigme MVC en exploitant la problématique de la fourchette 
 */
using System;
using System.Windows.Forms;

namespace MVC_Fourchette_BSI
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Des trucs pour l'UI
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            // Les fondamentaux MVC (créations et liens entre eux) 
            CtrlFork control = new CtrlFork();
            ModelFork model = new ModelFork(control);
            FrmFork view = new FrmFork(control);

            // Mise en route de la logique de l'application => son état initial
            control.Init();

            // Début de l'application en affichant la vue
            Application.Run(view);
        }
    }
}
